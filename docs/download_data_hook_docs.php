<?php
/* $Id:*/

/**
 * @file
 * Documentation for hooks provided by the download_data module.
 *
 * The download_data module provides extendable functionality through hooks
 * documented in this file.
 */

/**
 * Acts on downloads defined by other modules.
 *
 * Access to a download, column headings, and data are all defined via this hook.
 *
 * @param $op
 *   What kind of action is being performed. Possible values:
 *   - "access": access permissions for this download are being checked.
 *   - "settings": download settings are defined, such as the file name
 *   - "row_main_data": main data of download is collected as a single database resource.
 *     Note: at this stage data is still in the database, only a pointer to the data is available.
 *   - "row_additional_data": a row of data has been collected, add additional data to the row.
 * @param $variables
 *   Information about this download which has been passed in as variables in the URL requesting the file,
 *   might include: nid, report type, or anything else that's required.
 * @param $settings  
 *   Settings for the download to be defined during the settings operation,
 *   might include: the file name, column headings, or anything else that's required.
 * @param &$data
 *   A single row of data collected from the database after operation "row_main_data".
 * @param $counter 
 *   The counter restarts downloads from the last row fetched when the file is created during cron runs.
 *   Used during the "row_main_data" operation. Include $counter in the database query that collects the main data for the download.
 *   The counter is numeric, and must reference a numeric field of the database query.
 *     
 * @return
 *   This varies depending on the operation. 
 *   - "access": TRUE or FALSE (Boolean)
 *   - "settings": an object which can contain any settings. These elements are required:
 *                 ->counter_field: defines which (numeric) database field is to be used as a counter (String).
 *                 ->safe_fields: data fields which don't need to be sanitised (Array).
 *                 ->file_name: the name of the file to be output (String).
 *                 ->headings: columns to be output in file. Array keys are field names, array values are column labels (Array).
 *   - "row_main_data": results of a database query that has looked up the main data for the download (Resource Pointer).
 *   - "row_additional_data": the data with additional data included (Array).  
 */
function report_download_data($op, $variables = array(), $settings = NULL, &$data = NULL, $counter = 0) {

	// note: a variable identifying different types of downloads could be included in $variable,
	// at this point a check could be made, and one download definition could be selected if more than one has been defined.  

  // definition for a download	 
	switch ($op) {
		case 'access':
			return user_access('download example');
			
		case 'settings':
			$settings->counter_field = 'uid';
			$settings->safe_fields = array('uid', 'username', 'email');
      $settings->file_name = 'Users_' . date("Y-m-d_H\hi\m") . '.csv'; 

			// get column headings
			$settings->headings = array('uid' => t('UID'), 'username' => t('User Name'), 'email' => t('Email Address'), 'roles' => t('Roles'));
			
			return $settings;
			 
		case 'row_main_data':
			// get data rows (accounts)										
		  $results = db_query("SELECT u.uid, u.name AS username, u.mail AS email     
													 FROM {users} u 
													 WHERE u.uid > %d", $counter);
			 return $results;
			
		case 'row_additional_data':

			$roles = array('2' => 'authenticated user');
			$result = db_query('SELECT r.rid, r.name FROM {role} r INNER JOIN {users_roles} ur ON ur.rid = r.rid WHERE ur.uid = %d', $uid);

		  while ($role = db_fetch_object($result)) {
		    $roles[$role->rid] = $role->name;
		  }

			$data->roles = implode(' | ', $roles); 

			$data = (array) $data;

		  return $data; 
	} 
}