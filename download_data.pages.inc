<?php

/**
 * Triggered by 'Generate New Report' link.
 * @param module  
 * The module using Download Data APIs.
 * @param method  
 * Generation method: immediate, or cron.
 * @return None.
 */
function download_data_process($module = NULL, $method = 'immediate') {

  // fail gracefully if no module is specified
  if (empty($module)) {
    drupal_not_found();
    return;
  }
 
  // get variables
  $variables = $_GET;
  unset($variables['q']);
  
  // check user has access perm to file
  $access = module_invoke($module, 'download_data', 'access', $variables, $settings, $data);
  
  if (!$access) {
    drupal_access_denied();
    return;
  }

  // decide download method
  switch ($method) {
    case 'immediate':
      download_data_immediate_download($module, $variables);
      break;
    case 'cron':
      $file_id = download_data_cron_generated($module, $variables);
      break; 
    default:
      download_data_immediate_download($module, $variables);
      break;
  }

  return;
}

/**
 *  download file to user
 *
 * @param file_id
 * ID of file in database
 * @return Output the file.
 * 
 */
function download_data_file_download($file_id) {
   
  // get info from db
  $file = db_fetch_object(db_query("SELECT * FROM {download_data_files} WHERE file_id = %d LIMIT 1", $file_id));
  
  parse_str($file->variables, $variables);
 
  // check access
  $access = module_invoke($file->module, 'download_data', 'access', $variables);
 
  if (!$access) {
    drupal_access_denied();
    return;
  }
  
  $file_path_parts = explode('/', $file->file_path);
  $file_name = $file_path_parts[count($file_path_parts) - 1];
  drupal_set_header('Content-Type: text/x-comma-separated-values');
  drupal_set_header('Content-Disposition: attachment; filename="' . $file_name . '"');
  
  $handle = fopen($file->file_path, "r");
  if ($handle) {
    while (!feof($handle)) {
      $buffer = fgets($handle, 4096);
      echo $buffer;
    }
    fclose($handle);
  }
  
  download_data_immediate_end();
  
  return;
}