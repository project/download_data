********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Download Data Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Enables data (specified by other modules) to be downloaded as a file.

This module provides methods of making the download scalable:

  Method 1: Immediate download
            Starts sending the file immediately. 
            File rows are output as they are calculated.
            The limit is the max execution time limit for PHP scripts.

  Method 2: Create file using cron
            File is created on cron run(s). 
            The file is not available until it's finished being created. 
            There is no software limit on the size of the file.
            Files are either buffered through drupal (default) which will have
            the limit of php timeout for the file to be bufferred or can be
            set for direct download in which case no security checks are done
            by drupal but the size of file is only limited by apache and the
            browser.
            NOTE: For direct download it is either up to the individual
            module to secure (e.g. add a default .htaccess file) OR the site
            administrator must do this as they see fit. 

********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire module directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.
  
  
********************************************************************
USAGE:

See the include /docs files for API documentation. 



********************************************************************
TO DO:        

* register_shutdown_function()
http://uk3.php.net/manual/en/function.register-shutdown-function.php

Currently the counter is saved to the database each time that a row
of data is output, so that if the script times out, it will continue 
from where it stopped.

It may be possible to use register_shutdown_function() to set the 
counter just once at the end of a time out, avoiding a lot of writing 
to the database.

* Immediate Download
Make the immediate download method more robust by using Drupal 6 API
'Batch operations: Progressbar for heavy computations'
http://drupal.org/node/114774#batch

(when module is updated to Drupal 6)

 


********************************************************************
AUTHOR CONTACT

http://drupal.org/user/3555/contact

        
********************************************************************
ACKNOWLEDGEMENT

Download Data module was developed for BrightTalk by Code Positive Ltd.
   
http://www.brighttalk.com
http://www.codepositive.com

