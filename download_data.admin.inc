<?php

/**
 * Callback for admin settings gets this form.
 */
function download_data_admin_settings() {  
  $form = array();
  $form['download_data_cron_batch_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch Size'),
    '#description' => t('The max rows is the number that the last batch will
                        stop after, i.e. if batches of 10000 and max rows 15000
                        then max total batch = 20000'),
    '#default_value' => variable_get('download_data_cron_batch_size', 15000),
  );

  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Download Ready Email'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['email']['send_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send Email'),
    '#default_value' => variable_get('download_data_ready_email_send', 0), 
  );

  $form['email']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('Tokens that can be included: !download_title, !download_path.'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('download_data_ready_email_message', download_data_ready_email_message_default()),
  );

  download_data_configuration($form);
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
  );

  return $form;
}

function download_data_admin_settings_submit($form, &$form_state) {
  variable_set('download_data_by_download_maximums', $form_state['values']['by_download']);
  variable_set('download_data_ready_email_message', $form_state['values']['message']);
  variable_set('download_data_ready_email_send', $form_state['values']['send_email']);
  variable_set('download_data_cron_batch_size', $form_state['values']['download_data_cron_batch_size']);
}

